![118aed9.jpg](https://bitbucket.org/repo/GMBMob/images/510355300-118aed9.jpg)
In the start of 20th century, the word digital was unknown to the whole world. But today, everything is stored, processed and managed in long sequence of numbers. 

Binary numbers only have 0 & 1's. Before binary numbers analog systems were used and there was huge issue of amplification, synchronization, confusion and many other issues. After the introduction of digital world, all of these issues were solved as only 0 & 1 were there. 

This program is simple implementation of addition of binary number that is allot different from decimal addition.

If you want to learn more about the binary numbers. [Visit website](http://www.gamucci.com) that contains information.